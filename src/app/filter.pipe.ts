import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

    transform(clients: any, filter: any): any {
        if (filter === undefined) { return clients; }
        return clients.filter(function (client: any) {
            return client.general.firstName.toString().toLowerCase().includes(filter.toLowerCase());
        });
    }

}
