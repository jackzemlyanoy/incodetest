import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/internal/operators'

@Injectable({
  providedIn: 'root'
})
export class ClientsListService {
// адрес сервиса
    private url = 'http://localhost:4200/assets/clients.json';

    constructor(private http: HttpClient) {
    }

    // Отправка GET запроса нв сервер
    public getProducts(url) {
        return this.http.get(url)
            .pipe(
                // catchError(this.handleError),
                map((result: any) => result)
            );
    }
}
