import { Component, OnInit } from '@angular/core';
import {ClientsListService} from '../shared/clients-list.service';
import { Clients } from '../shared/client.model';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.css',
      '../../../node_modules/font-awesome/css/font-awesome.css'
  ]
})
export class ClientsListComponent implements OnInit {

  clients: Clients[];
  getId: any;

  constructor(private service: ClientsListService) { }

  private getClients() {
      this.service.getProducts('http://localhost:4200/assets/clients.json').subscribe(
          (items) => {
              this.clients = items;
              console.log(this.clients);
          },
          // (items)=>{
          //     if(items.length == undefined){
          //         alert("Can`t get data");
          //     }
          // }
      );
  }

    getClientsId(i) {
        this.getId = this.clients;
        this.getId = i;
        console.log(i);
    }


  ngOnInit() {
      this.getClients();
  }

}
